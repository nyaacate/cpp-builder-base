FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN set -eux && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        bash \
        ca-certificates \
        curl \
        gnupg \
        tar \
        unzip \
        wget \
        zip \
    && \
    \
    wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - && \
    echo "deb http://apt.llvm.org/bullseye/ llvm-toolchain-bullseye-12 main" > /etc/apt/sources.list.d/llvm-12.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        clang-12 \
        clang-format-12 \
        clang-tidy-12 \
        clang-tools-12 \
        clangd-12 \
        cmake \
        git \
        libc++-12-dev \
        libc++abi-12-dev \
        lld-12 \
        lldb-12 \
        ninja-build \
        pkg-config \
    && \
    \
    update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-12 120 && \
    update-alternatives --install /usr/bin/cc cc /usr/bin/clang-12 120 && \
    update-alternatives --install /usr/bin/clang clang /usr/bin/clang-12 120 && \
    update-alternatives --install /usr/bin/clang-cl clang-cl /usr/bin/clang-cl-12 120 && \
    update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-12 120 && \
    \
    update-alternatives --install /usr/bin/clang-format clang-format /usr/bin/clang-format-12 120 && \
    update-alternatives --install /usr/bin/clang-tidy clang-tidy /usr/bin/clang-tidy-12 120 && \
    update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-12 120 && \
    \
    git clone https://github.com/Microsoft/vcpkg.git /vcpkg && \
    /vcpkg/bootstrap-vcpkg.sh -disableMetrics -useSystemBinaries && \
    update-alternatives --install /usr/bin/vcpkg vcpkg /vcpkg/vcpkg 100 && \
    vcpkg integrate install && \
    clang --version

